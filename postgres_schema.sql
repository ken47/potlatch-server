--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: authorities; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE authorities (
    id bigint NOT NULL,
    authority text NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.authorities OWNER TO postgres;

--
-- Name: authorities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE authorities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.authorities_id_seq OWNER TO postgres;

--
-- Name: authorities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE authorities_id_seq OWNED BY authorities.id;


--
-- Name: gift_chains; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE gift_chains (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.gift_chains OWNER TO postgres;

--
-- Name: gift_chains_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gift_chains_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gift_chains_id_seq OWNER TO postgres;

--
-- Name: gift_chains_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE gift_chains_id_seq OWNED BY gift_chains.id;


--
-- Name: gifts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE gifts (
    image_path text,
    title text NOT NULL,
    caption text,
    user_id bigint NOT NULL,
    gift_chain_id bigint,
    id bigint NOT NULL,
    touched_count bigint DEFAULT 0 NOT NULL,
    flagged_count bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.gifts OWNER TO postgres;

--
-- Name: gift_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gift_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gift_id_seq OWNER TO postgres;

--
-- Name: gift_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE gift_id_seq OWNED BY gifts.id;


--
-- Name: oauth_access_token; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_access_token (
    token_id character varying(256),
    token bytea,
    authentication_id character varying(256),
    user_name character varying(256),
    client_id character varying(256),
    authentication bytea,
    refresh_token character varying(256)
);


ALTER TABLE public.oauth_access_token OWNER TO postgres;

--
-- Name: oauth_approvals; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_approvals (
    userid character varying(256),
    clientid character varying(256),
    scope character varying(256),
    status character varying(10),
    expiresat timestamp without time zone,
    lastmodifiedat timestamp without time zone
);


ALTER TABLE public.oauth_approvals OWNER TO postgres;

--
-- Name: oauth_client_details; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_client_details (
    client_id character varying(256) NOT NULL,
    resource_ids character varying(256),
    client_secret character varying(256),
    scope character varying(256),
    authorized_grant_types character varying(256),
    web_server_redirect_uri character varying(256),
    authorities character varying(256),
    access_token_validity integer,
    refresh_token_validity integer,
    additional_information character varying(4096),
    autoapprove character varying(256)
);


ALTER TABLE public.oauth_client_details OWNER TO postgres;

--
-- Name: oauth_client_token; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_client_token (
    token_id character varying(256),
    token bytea,
    authentication_id character varying(256),
    user_name character varying(256),
    client_id character varying(256)
);


ALTER TABLE public.oauth_client_token OWNER TO postgres;

--
-- Name: oauth_code; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_code (
    code character varying(256),
    authentication bytea
);


ALTER TABLE public.oauth_code OWNER TO postgres;

--
-- Name: oauth_refresh_token; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_refresh_token (
    token_id character varying(256),
    token bytea,
    authentication bytea
);


ALTER TABLE public.oauth_refresh_token OWNER TO postgres;

--
-- Name: user_gift_locks; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_gift_locks (
    user_id bigint,
    gift_id bigint,
    operation text,
    created bigint
);


ALTER TABLE public.user_gift_locks OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id bigint NOT NULL,
    username text NOT NULL,
    password text NOT NULL,
    salt text NOT NULL,
    email text NOT NULL,
    touched_count bigint DEFAULT 0 NOT NULL,
    gift_id bigint,
    flagged_count bigint DEFAULT 0 NOT NULL,
    update_interval smallint DEFAULT 60 NOT NULL,
    profile_pic_id bigint,
    should_hide_flagged boolean DEFAULT false NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: COLUMN users.update_interval; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN users.update_interval IS 'how often touched counts are updated, in minutes';


--
-- Name: COLUMN users.profile_pic_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN users.profile_pic_id IS 'one of the user''s gifts';


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: users_x_gift_flags; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users_x_gift_flags (
    id bigint NOT NULL,
    user_id bigint,
    gift_id bigint
);


ALTER TABLE public.users_x_gift_flags OWNER TO postgres;

--
-- Name: users_x_gift_flags_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_x_gift_flags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_x_gift_flags_id_seq OWNER TO postgres;

--
-- Name: users_x_gift_flags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_x_gift_flags_id_seq OWNED BY users_x_gift_flags.id;


--
-- Name: users_x_gift_touches; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users_x_gift_touches (
    id bigint NOT NULL,
    user_id bigint,
    gift_id bigint
);


ALTER TABLE public.users_x_gift_touches OWNER TO postgres;

--
-- Name: users_x_gift_touches_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_x_gift_touches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_x_gift_touches_id_seq OWNER TO postgres;

--
-- Name: users_x_gift_touches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_x_gift_touches_id_seq OWNED BY users_x_gift_touches.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authorities ALTER COLUMN id SET DEFAULT nextval('authorities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gift_chains ALTER COLUMN id SET DEFAULT nextval('gift_chains_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gifts ALTER COLUMN id SET DEFAULT nextval('gift_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_x_gift_flags ALTER COLUMN id SET DEFAULT nextval('users_x_gift_flags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_x_gift_touches ALTER COLUMN id SET DEFAULT nextval('users_x_gift_touches_id_seq'::regclass);


--
-- Name: authorities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY authorities
    ADD CONSTRAINT authorities_pkey PRIMARY KEY (id);


--
-- Name: gift_chains_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY gift_chains
    ADD CONSTRAINT gift_chains_pkey PRIMARY KEY (id);


--
-- Name: gift_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY gifts
    ADD CONSTRAINT gift_pkey PRIMARY KEY (id);


--
-- Name: oauth_client_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_client_details
    ADD CONSTRAINT oauth_client_details_pkey PRIMARY KEY (client_id);


--
-- Name: user_gift_locks_user_id_gift_id_operation_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_gift_locks
    ADD CONSTRAINT user_gift_locks_user_id_gift_id_operation_key UNIQUE (user_id, gift_id, operation);


--
-- Name: users_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- Name: users_x_gift_flags_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users_x_gift_flags
    ADD CONSTRAINT users_x_gift_flags_pkey PRIMARY KEY (id);


--
-- Name: users_x_gift_flags_user_id_gift_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users_x_gift_flags
    ADD CONSTRAINT users_x_gift_flags_user_id_gift_id_key UNIQUE (user_id, gift_id);


--
-- Name: users_x_gift_touches_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users_x_gift_touches
    ADD CONSTRAINT users_x_gift_touches_pkey PRIMARY KEY (id);


--
-- Name: users_x_gift_touches_user_id_gift_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users_x_gift_touches
    ADD CONSTRAINT users_x_gift_touches_user_id_gift_id_key UNIQUE (user_id, gift_id);


--
-- Name: authorities_authority_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX authorities_authority_idx ON authorities USING btree (authority);


--
-- Name: fki_gift_chain_to_user_id_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_gift_chain_to_user_id_fk ON gift_chains USING btree (user_id);


--
-- Name: fki_gift_user_id_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_gift_user_id_fk ON gifts USING btree (user_id);


--
-- Name: fki_user_id_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_user_id_fk ON authorities USING btree (user_id);


--
-- Name: fki_user_to_primary_gift_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_user_to_primary_gift_id ON users USING btree (profile_pic_id);


--
-- Name: fki_users_x_gift_flags_gift_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_users_x_gift_flags_gift_id ON users_x_gift_flags USING btree (gift_id);


--
-- Name: fki_users_x_gift_flags_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_users_x_gift_flags_user_id ON users_x_gift_flags USING btree (user_id);


--
-- Name: fki_users_x_gift_touches_gift_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_users_x_gift_touches_gift_id ON users_x_gift_touches USING btree (gift_id);


--
-- Name: fki_users_x_gift_touches_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_users_x_gift_touches_user_id ON users_x_gift_touches USING btree (user_id);


--
-- Name: gift_chains_name_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX gift_chains_name_idx ON gift_chains USING btree (name);


--
-- Name: gift_gift_chain_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX gift_gift_chain_id_idx ON gifts USING btree (gift_chain_id);


--
-- Name: gift_title_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX gift_title_idx ON gifts USING btree (title);


--
-- Name: gift_chain_to_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gift_chains
    ADD CONSTRAINT gift_chain_to_user_id_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: gift_to_gift_chain_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gifts
    ADD CONSTRAINT gift_to_gift_chain_fk FOREIGN KEY (gift_chain_id) REFERENCES gift_chains(id);


--
-- Name: gift_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gifts
    ADD CONSTRAINT gift_user_id_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authorities
    ADD CONSTRAINT user_id_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: user_to_primary_gift_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT user_to_primary_gift_id FOREIGN KEY (profile_pic_id) REFERENCES gifts(id);


--
-- Name: users_x_gift_flags_gift_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_x_gift_flags
    ADD CONSTRAINT users_x_gift_flags_gift_id FOREIGN KEY (gift_id) REFERENCES gifts(id);


--
-- Name: users_x_gift_flags_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_x_gift_flags
    ADD CONSTRAINT users_x_gift_flags_user_id FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: users_x_gift_touches_gift_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_x_gift_touches
    ADD CONSTRAINT users_x_gift_touches_gift_id FOREIGN KEY (gift_id) REFERENCES gifts(id);


--
-- Name: users_x_gift_touches_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_x_gift_touches
    ADD CONSTRAINT users_x_gift_touches_user_id FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

