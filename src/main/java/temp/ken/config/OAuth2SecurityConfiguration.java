package temp.ken.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.builders.InMemoryClientDetailsServiceBuilder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.expression.OAuth2WebSecurityExpressionHandler;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import temp.ken.models.User;
import temp.ken.security.CustomUserDetailsService;
import temp.ken.security.PasswordHash;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *	Configure this web application to use OAuth 2.0.
 *
 * 	The resource server is located at "/video", and can be accessed only by retrieving a token from "/oauth/token"
 *  using the Password Grant Flow as specified by OAuth 2.0.
 *
 *  Most of this code can be reused in other applications. The key methods that would definitely need to
 *  be changed are:
 *
 *  ResourceServer.configure(...) - update this method to apply the appropriate
 *  set of scope requirements on client requests
 *
 *  OAuth2Config constructor - update this constructor to create a "real" (not hard-coded) UserDetailsService
 *  and ClientDetailsService for authentication. The current implementation should never be used in any
 *  type of production environment as these hard-coded credentials are highly insecure.
 *
 *  OAuth2SecurityConfiguration.containerCustomizer(...) - update this method to use a real keystore
 *  and certificate signed by a CA. This current version is highly insecure.
 *
 */
@Configuration
public class OAuth2SecurityConfiguration {

    // This first section of the configuration just makes sure that Spring Security picks
    // up the UserDetailsService that we create below.
    @Configuration
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled=true)
    protected static class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

        @Autowired
        private UserDetailsService userDetailsService;

        @Autowired
        protected void registerAuthentication(
                final AuthenticationManagerBuilder auth) throws Exception {

            // this song and dance ensures that the proper salt source is set for password encoding
            DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
            provider.setSaltSource(new User.UserSaltSource());
            provider.setUserDetailsService(userDetailsService);
            provider.setPasswordEncoder(getPasswordEncoder());
            auth.authenticationProvider(provider);
        }

        @Autowired
        DataSource dataSource;

        /**
         * http://www.mkyong.com/spring-security/spring-security-password-hashing-example/
         *
         * @param auth
         * @throws Exception
         */
        @Override
        public void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userDetailsService);
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {

            http.csrf().disable()
                    .authorizeRequests()
//                    .anyRequest().authenticated()
                    .antMatchers(HttpMethod.POST, "/register").permitAll()
                    .antMatchers("/favicon.ico", "/").permitAll()
                    .antMatchers("/goodbye").permitAll()
                    .antMatchers(HttpMethod.POST, "/oauth/authorize").permitAll();
        }

        // TODO: is this bean annotation necessary?
        @Bean
        public PasswordEncoder getPasswordEncoder(){
            return new PasswordHash();
        }
    }

    private static final AuthenticationSuccessHandler NO_REDIRECT_SUCCESS_HANDLER = new AuthenticationSuccessHandler() {
        @Override
        public void onAuthenticationSuccess(HttpServletRequest request,
                                            HttpServletResponse response, Authentication authentication)
                throws IOException, ServletException {
            response.setStatus(200);
        }
    };

    /**
     *	This method is used to configure who is allowed to access which parts of our
     *	resource server (i.e. the "/video" endpoint)
     */
    @Configuration
    @EnableResourceServer
    @Order(1)
    protected static class ResourceServer extends
            ResourceServerConfigurerAdapter {

        // This method configures the OAuth scopes required by clients to access
        // all of the paths in the video service.
        @Override
        public void configure(HttpSecurity http) throws Exception {

            http.csrf().disable()
                .authorizeRequests()
                // TODO: figure out how to leverage @PreAuthorize
                .antMatchers(HttpMethod.GET, "/users/**/*").access("#oauth2.hasScope('read') and hasRole('ROLE_USER')")
                .antMatchers(HttpMethod.GET, "/gifts/**/*").access("#oauth2.hasScope('read') and hasRole('ROLE_USER')")
                .antMatchers(HttpMethod.POST, "/users/**/*").access("#oauth2.hasScope('write') and hasRole('ROLE_USER')")
                .antMatchers(HttpMethod.POST, "/gifts/**/*").access("#oauth2.hasScope('write') and hasRole('ROLE_USER')")
                .antMatchers(HttpMethod.PUT, "/**/*").access("#oauth2.hasScope('write') and hasRole('ROLE_USER')")
                .antMatchers(HttpMethod.DELETE, "/**/*").access("#oauth3.hasScope('write') and hasRole('ROLE_USER')");
        }

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
            resources.resourceId("ohai");
        }

    }


    /**
     * This class is used to configure how our authorization server (the "/oauth/token" endpoint)
     * validates client credentials.
     */
    @Configuration
    @EnableAuthorizationServer
    @Order(Ordered.LOWEST_PRECEDENCE - 100)
    protected static class OAuth2Config extends
            AuthorizationServerConfigurerAdapter {

        // Delegate the processing of Authentication requests to the framework
        @Autowired
        private AuthenticationManager authenticationManager;

        // A data structure used to store both a ClientDetailsService and a UserDetailsService
        private UserDetailsService userDetailsService;

        private ClientDetailsService clientDetailsService;

        /**
         *
         * This constructor is used to setup the clients and users that will be able to login to the
         * system. This is a VERY insecure setup that is using hard-coded lists of clients / users /
         * passwords and should never be used for anything other than local testing
         * on a machine that is not accessible via the Internet. Even if you use
         * this code for testing, at the bare minimum, you should consider changing the
         * passwords listed below and updating the VideoSvcClientApiTest.
         *
         * @throws Exception
         */
        public OAuth2Config() throws Exception {

            // If you were going to reuse this class in another
            // application, this is one of the key sections that you
            // would want to change
            // Create a service that has the credentials for all our clients
            clientDetailsService = new InMemoryClientDetailsServiceBuilder()
                    // Create a client that has "read" and "write" access to the
                    // video service
                    // REFRESH TOKEN INFO: http://techannotation.wordpress.com/2014/04/29/5-minutes-with-spring-oauth-2-0/
                    .withClient("web").authorizedGrantTypes("password")
                    .authorities("ROLE_TESTING", "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT")
                    .secret("secret")
                    .scopes("read", "write", "test").resourceIds("ohai")
                    .accessTokenValiditySeconds(3600)
                    .and()
                            // Create a second client that only has "read" access to the
                            // video service
                    .withClient("mobileReader").authorizedGrantTypes("password")
                    .authorities("ROLE_CLIENT")
                    .secret("secret")
                    .scopes("read").resourceIds("test")
                    .accessTokenValiditySeconds(3600).and().build();

            userDetailsService = new CustomUserDetailsService();
        }

        @Autowired
        private DataSource dataSource;

        @Bean
        public TokenStore getTokenStore() {
            return new JdbcTokenStore(dataSource);
        }

        /**
         * Return the list of trusted client information to anyone who asks for it.
         */
        @Bean
        public ClientDetailsService clientDetailsService() throws Exception {
            return clientDetailsService;
        }

        /**
         * Return all of our user information to anyone in the framework who requests it.
         */
        @Bean
        public UserDetailsService userDetailsService() {
            return userDetailsService;
        }

        /**
         * This method tells our AuthorizationServerConfigurerAdapter to use the delegated AuthenticationManager
         * to process authentication requests.
         */
        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints)
                throws Exception {
            endpoints.authenticationManager(authenticationManager)
                    .tokenStore(getTokenStore());
        }


        /**
         * This method tells the AuthorizationServerConfigurerAdapter to use our self-defined client details service to
         * authenticate clients with.
         */
        @Override
        public void configure(ClientDetailsServiceConfigurer clients)
                throws Exception {
            clients.withClientDetails(clientDetailsService());
        }

    }
}
