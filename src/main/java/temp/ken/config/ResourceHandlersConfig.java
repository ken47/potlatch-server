package temp.ken.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
public class ResourceHandlersConfig extends WebMvcConfigurationSupport {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //TODO: spring boot claims it automatically adds this...but it doesn't work in jar form
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/favicon.ico").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("**/*.html").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("**/*.jpg").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("**/*.png").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("**/*.gif").addResourceLocations("classpath:/static/");
    }

    /**
     * serves "/index.html" by default when visiting "/"
     */
    @Controller
    static class IndexController {
        @RequestMapping("/")
        String index() {
            return "forward:/index.html";
        }
    }

    /**
     * this is needed for "forward:/index.html" to work
     * @return
     */
    @Bean
    public ViewResolver getViewResolver(){
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/static/");
        resolver.setSuffix(".html");
        return resolver;
    }
}
