package temp.ken.helpers;

/**
 *
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import org.springframework.web.multipart.MultipartFile;
import temp.ken.models.Gift;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import org.apache.commons.io.FilenameUtils;


/**
 * This class provides a simple implementation to store video binary
 * data on the file system in a "videos" folder. The class provides
 * methods for saving videos and retrieving their binary data.
 *
 * @author jules
 *
 */
public class GiftFileManager {

    /**
     * This static factory method creates and returns a
     * GiftFileManager object to the caller. Feel free to customize
     * this method to take parameters, etc. if you want.
     *
     * @return
     * @throws IOException
     */
    public static GiftFileManager get() throws IOException {
        return new GiftFileManager();
    }

    private Path targetDir_ = Paths.get("src/main/resources/static/images");

    // The GiftFileManager.get() method should be used
    // to obtain an instance
    private GiftFileManager() throws IOException{
        if(!Files.exists(targetDir_)){
            Files.createDirectories(targetDir_);
        }
    }

    // Private helper method for resolving video file paths
    private Path getExistingPath(Gift g){
        assert(g != null);

        return targetDir_.resolve(getFilename(g));
    }

    private Path getPath(Gift g){
        assert(g != null);

        return targetDir_.resolve(getFilename(g));
    }

    private String getFilename(Gift g) {
        System.out.println("111: " + g.getImagePath());
        System.out.println("222: " + FilenameUtils.getExtension(g.getImagePath()));
        return "gift_" + g.getId() + "." + FilenameUtils.getExtension(g.getImagePath());
    }

    /**
     * This method returns true if the specified Video has binary
     * data stored on the file system.
     *
     * @param g
     * @return
     */
    public boolean hasImage(Gift g){
        Path source = getExistingPath(g);
        return Files.exists(source);
    }

    /**
     * This method copies the binary data for the given video to
     * the provided output stream. The caller is responsible for
     * ensuring that the specified Video has binary data associated
     * with it. If not, this method will throw a FileNotFoundException.
     *
     * @param g
     * @param out
     * @throws IOException
     */
    public void copyGiftData(Gift g, OutputStream out) throws IOException {
        Path source = getExistingPath(g);
        if(!Files.exists(source)){
            throw new FileNotFoundException("Unable to find the referenced video file for videoId:"+g.getId());
        }
        Files.copy(source, out);
    }

    /**
     * This method reads all of the data in the provided InputStream and stores
     * it on the file system. The data is associated with the Video object that
     * is provided by the caller.
     *
     * @param g
     * @param g
     * @throws IOException
     */
    public String saveGiftData(Gift g, MultipartFile file) throws IOException{
        assert(g != null);

        System.out.println(file.getOriginalFilename());
        g.setImagePath(file.getOriginalFilename());
        Path target = getPath(g);
        Files.copy(file.getInputStream(), target, StandardCopyOption.REPLACE_EXISTING);
        return getFilename(g);
    }
}
