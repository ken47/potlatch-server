package temp.ken;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.sql.DataSource;
import java.util.Properties;

//Tell Spring to automatically inject any dependencies that are marked in
//our classes with @Autowired
@EnableAutoConfiguration
//@EnableJpaRepositories(basePackageClasses = {VideoRepository.class, VideoRepository2.class})
// for more info: http://docs.spring.io/spring-data/jpa/docs/current/reference/html/
@EnableJpaRepositories
// Tell Spring that this object represents a Configuration for the
// application
@Configuration
// Tell Spring to turn on WebMVC (e.g., it should enable the DispatcherServlet
// so that requests can be routed to our Controllers)
@EnableWebMvc
// Tell Spring to go and scan our controller package (and all sub packages) to
// find any Controllers or other components that are part of our applciation.
// Any class in this package that is annotated with @Controller is going to be
// automatically discovered and connected to the DispatcherServlet.
@ComponentScan
public class Application {
    @Autowired
    DataSource dataSource;

    // Tell Spring to launch our app!
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

//    http://www.mkyong.com/spring-security/spring-security-hibernate-annotation-example/
//    @Bean
//    public SessionFactory sessionFactory() {
//        LocalSessionFactoryBuilder builder =
//                new LocalSessionFactoryBuilder(dataSource);
//        builder.scanPackages("temp.ken.models")
//                .addProperties(getHibernateProperties());
//
//        return builder.buildSessionFactory();
//    }
//
//    private Properties getHibernateProperties() {
//        Properties prop = new Properties();
//        prop.put("hibernate.format_sql", "true");
//        prop.put("hibernate.show_sql", "true");
//        prop.put("hibernate.dialect",
//                "org.hibernate.dialect.PostgreSQLDialect");
//        return prop;
//    }
}


