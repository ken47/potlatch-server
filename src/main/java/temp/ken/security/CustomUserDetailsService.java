package temp.ken.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import temp.ken.models.User;
import temp.ken.repositories.OAuthTokenRepo;
import temp.ken.repositories.UserRepo;

import java.util.Arrays;
import java.util.Collection;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws
            UsernameNotFoundException {
        User u = userRepo.findByUsername(username);
        if (u == null) {
            throw new UsernameNotFoundException("Username " + username + " not found");
        }
        return u;
    }

    // TODO: definitely fix this!
    private Collection<? extends GrantedAuthority> getGrantedAuthorities(String username) {
        Collection<? extends GrantedAuthority> authorities;
        // todo: use something sane here
        if (username.equals("John")) {
            authorities = Arrays.asList(() -> "ROLE_ADMIN", () -> "ROLE_BASIC");
        } else {
            authorities = Arrays.asList(() -> "ROLE_BASIC");
        }
        return authorities;
    }
}
