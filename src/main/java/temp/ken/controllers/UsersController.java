package temp.ken.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import temp.ken.models.Gift;
import temp.ken.models.GiftChain;
import temp.ken.models.User;
import temp.ken.repositories.GiftChainRepo;
import temp.ken.repositories.GiftRepo;
import temp.ken.repositories.UserRepo;

import java.security.Principal;
import java.util.List;
import java.util.Set;

@Controller
public class UsersController {
    @Autowired
    UserRepo userRepo;

    @Autowired
    GiftRepo giftRepo;

    @Autowired
    GiftChainRepo giftChainRepo;

    @RequestMapping(
            value = "/users/{username}",
            produces="application/json",
            method= RequestMethod.GET
    )
    @ResponseBody
    public User me(@PathVariable("username") String username) {
        return userRepo.findByUsername(username);
    }

    @RequestMapping(
            value = "/users/{username}/gifts",
            produces="application/json",
            method= RequestMethod.GET
    )
    @ResponseBody
    public List<Gift> userGifts(@PathVariable("username") String username) {
        return giftRepo.findByUsername(username);
    }

    @RequestMapping(
            value = "/users/{username}/giftChains",
            produces="application/json",
            method= RequestMethod.GET
    )
    @ResponseBody
    public List<GiftChain> userGiftChains(@PathVariable("username") String username) {
        return giftChainRepo.findByUsername(username);
    }

    @RequestMapping(
            value = "/users/me",
            produces="application/json",
            method= RequestMethod.GET
    )
    @ResponseBody
    public User me(Principal principal) {
        return userRepo.findByUsername(principal.getName());
    }

    @RequestMapping(
            value = "/users/me",
            produces="application/json",
            method= RequestMethod.POST
    )
    @ResponseBody
    public ResponseEntity<String> updateMe(@RequestBody User user, Principal principal) {
        User realUser = userRepo.findByUsername(principal.getName());
        if (user.getId() != realUser.getId()) {
            return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
        }
        // can be optimized
        realUser.setUpdateInterval(user.getUpdateInterval());
        realUser.setShouldHideFlagged(user.getShouldHideFlagged());
        userRepo.save(realUser);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(
            value = "/users/getTopTwenty",
            produces="application/json",
            method= RequestMethod.GET
    )
    @ResponseBody
    public Set<User> mostTouched() {
        return userRepo.findTwentyMostTouched();
    }
}
