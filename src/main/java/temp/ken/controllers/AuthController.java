package temp.ken.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import temp.ken.repositories.OAuthTokenRepo;
import temp.ken.security.PasswordHash;
import temp.ken.models.LoginDetail;
import temp.ken.models.User;
import temp.ken.repositories.UserRepo;

import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Logger;

/**
 * This simple VideoSvc allows clients to send HTTP POST requests with
 * videos that are stored in memory using a list. Clients can send HTTP GET
 * requests to receive a JSON listing of the videos that have been sent to
 * the controller so far. Stopping the controller will cause it to lose the history of
 * videos that have been sent to it because they are stored in memory.
 *
 * Notice how much simpler this VideoSvc is than the original VideoServlet?
 * Spring allows us to dramatically simplify our service. Another important
 * aspect of this version is that we have defined a VideoSvcApi that provides
 * strong typing on both the client and service interface to ensure that we
 * don't send the wrong paraemters, etc.
 *
 * This version of the VideoSvc uses @OneToMany for its Video/Category
 * operations.
 *
 * @author jules
 *
 */

// Tell Spring that this class is a Controller that should
// handle certain HTTP requests for the DispatcherServlet
@Controller
public class AuthController {
    @Autowired
    UserRepo userRepo;

    @Autowired
    OAuthTokenRepo oAuthTokenRepo;

    @RequestMapping(value="/register", method=RequestMethod.POST)
    @ResponseBody
    public boolean register(
            @RequestBody User u
    ) throws InvalidKeySpecException, NoSuchAlgorithmException, UnsupportedEncodingException {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[PasswordHash.SALT_BYTE_SIZE];
        random.nextBytes(salt);
        u.setSalt(new String(salt, Charset.forName("UTF-8")));
        u.setPassword(PasswordHash.createHash(u.getPassword(), u.getSalt()));
        userRepo.save(u);
        return true;
    }

    /**
     * http://blogrit.com/blog/Spring_MVC_-_Spring_Security_with_AJAX_and_JSON
     */
    /*
    @Autowired
    private AuthenticationManager authenticationManager;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public @ResponseBody
    LoginDetail login(@RequestBody User loginInfo) throws InvalidKeySpecException, NoSuchAlgorithmException {
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(loginInfo.getUsername(), loginInfo.getPassword());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new LoginDetail().success().principal(authentication.getName());
    }
    */

    @RequestMapping(value = "/goodbye")
    @ResponseBody
    public boolean logout(HttpSession session, Principal principal) {
        oAuthTokenRepo.deleteToken(principal.getName());
        session.invalidate();
        return true;
    }
}

