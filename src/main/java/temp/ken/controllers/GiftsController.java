package temp.ken.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import temp.ken.helpers.GiftFileManager;
import temp.ken.models.Gift;
import temp.ken.models.GiftChain;
import temp.ken.models.User;
import temp.ken.repositories.GiftChainRepo;
import temp.ken.repositories.GiftRepo;
import temp.ken.repositories.UserRepo;

import java.security.Principal;
import java.util.List;

@Controller
public class GiftsController {
    @Autowired
    GiftRepo giftRepo;

    @Autowired
    GiftChainRepo giftChainRepo;

    @Autowired
    UserRepo userRepo;

    /**
     * user cannot be touched by own gift
     *
     * @param giftId
     * @param principal
     * @return
     * @throws Exception
     */
    @RequestMapping(
            value = "/gifts/{id}/touch",
            produces="application/json",
            method=RequestMethod.POST
    )
    @ResponseBody
    public ResponseEntity<String> touchGift(
        @PathVariable("id") Long giftId,
        Principal principal
    ) throws Exception {
        User u = userRepo.findByUsername(principal.getName());
        Gift g = giftRepo.findById(giftId);
        if (g.getUser().getId() == u.getId()) {
            return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
        }
        giftRepo.touchGift(userRepo.findByUsername(principal.getName()).getId(), giftId, g.getUser().getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    // in millisecond
    private final static long TEN_SECONDS = 10000L;
    private final static String UNTOUCH_LOCK_PHRASE = "untouch_lock";
    private final static String UNFLAG_LOCK_PHRASE = "unflag_lock";

    @RequestMapping(
            value = "/gifts/{id}/untouch",
            produces="application/json",
            method=RequestMethod.POST
    )
    @ResponseBody
    public ResponseEntity<String> unTouchGift(
            @PathVariable("id") Long giftId,
            Principal principal
    ) throws Exception {
        User u = userRepo.findByUsername(principal.getName());
        Gift g = giftRepo.findById(giftId);
        if (g.getUser().getId() == u.getId()) {
            return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
        }

        java.util.Date date= new java.util.Date();
        Long now = System.currentTimeMillis() / 1000L;

        // this is just in case there's some sort of catastrophic technical failure that prevents the finally block from getting run
        giftRepo.freeOldLock(u.getId(), giftId, now - GiftsController.TEN_SECONDS, GiftsController.UNTOUCH_LOCK_PHRASE);

        giftRepo.setLock(u.getId(), giftId, now, GiftsController.UNTOUCH_LOCK_PHRASE);

        try {
            Long count = giftRepo.getTouchedCount(u.getId(), giftId);

            if (count > 0L) {
                giftRepo.unTouchGift(userRepo.findByUsername(principal.getName()).getId(), giftId, g.getUser().getId());
            }

            if (count == 0L) {
                return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
            }
        } catch(Exception e) {
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            giftRepo.freeLock(userRepo.findByUsername(principal.getName()).getId(), giftId, GiftsController.UNTOUCH_LOCK_PHRASE);
        }

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(
            value = "/gifts/{id}/flag",
            produces="application/json",
            method=RequestMethod.POST
    )
    @ResponseBody
    public ResponseEntity<String> flagGift(
            @PathVariable("id") Long giftId,
            Principal principal
    ) throws Exception {
        User u = userRepo.findByUsername(principal.getName());
        Gift g = giftRepo.findById(giftId);
        if (g.getUser().getId() == u.getId()) {
            return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
        }
        giftRepo.flagGift(userRepo.findByUsername(principal.getName()).getId(), giftId, g.getUser().getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(
            value = "/gifts/{id}/unflag",
            produces="application/json",
            method=RequestMethod.POST
    )
    @ResponseBody
    public ResponseEntity<String> unFlagGift(
            @PathVariable("id") Long giftId,
            Principal principal
    ) throws Exception {
        User u = userRepo.findByUsername(principal.getName());
        Gift g = giftRepo.findById(giftId);
        if (g.getUser().getId() == u.getId()) {
            return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
        }

        java.util.Date date = new java.util.Date();

        Long now = System.currentTimeMillis() / 1000L;

        // this is just in case there's some sort of catastrophic technical failure that prevents the finally block from getting run
        giftRepo.freeOldLock(u.getId(), giftId, now - GiftsController.TEN_SECONDS, GiftsController.UNFLAG_LOCK_PHRASE);

        giftRepo.setLock(u.getId(), giftId, now, GiftsController.UNFLAG_LOCK_PHRASE);

        try {
            Long count = giftRepo.getFlaggedCount(u.getId(), giftId);

            if (count > 0L) {
                giftRepo.unFlagGift(userRepo.findByUsername(principal.getName()).getId(), giftId, g.getUser().getId());
            }

            if (count == 0L) {
                return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
            }
        } catch(Exception e) {
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            giftRepo.freeLock(userRepo.findByUsername(principal.getName()).getId(), giftId, GiftsController.UNFLAG_LOCK_PHRASE);
        }

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(
            value = "/giftChains/{name}",
            produces="application/json",
            method=RequestMethod.POST
    )
    @ResponseBody
    public GiftChain saveGiftChain(@PathVariable("name") String name, Principal principal) throws Exception {
        GiftChain giftChain = new GiftChain();
        giftChain.setUser(userRepo.findByUsername(principal.getName()));
        giftChain.setName(name);
        return giftChainRepo.save(giftChain);
    }

    @RequestMapping(
            value = "/gifts/{id}",
            produces="application/json",
            method=RequestMethod.GET
    )
    @ResponseBody
    public Gift getGift(@PathVariable("id") Long id, Principal principal) throws Exception {
        User user = userRepo.findByUsername(principal.getName());
        Gift gift = giftRepo.findById(id);

        boolean touched = null != giftRepo.hasUserTouched(user.getId(), gift.getId());
        boolean flagged = null != giftRepo.hasUserFlagged(user.getId(), gift.getId());

        gift.setTouchedByCurrentUser(touched);
        gift.setFlaggedByCurrentUser(flagged);

        return gift;
    }

    @RequestMapping(
            value = "/gifts/findByTitleLikeIgnoreCase/{title}",
            produces="application/json",
            method=RequestMethod.GET
    )
    @ResponseBody
    public List<Gift> getMostRecent(@PathVariable("title") String title) throws Exception {
        return giftRepo.findByTitleLikeIgnoreCase(title + "%");
    }

    @RequestMapping(
            value = "/gifts/mostRecent",
            produces="application/json",
            method=RequestMethod.GET
    )
    @ResponseBody
    public List<Gift> getMostRecent() throws Exception {
        return giftRepo.getRecentGifts();
    }

    @RequestMapping(
            value = "/gifts/addToChain/{id}",
            produces="application/json",
            consumes="application/json",
            method=RequestMethod.POST
    )
    @ResponseBody
    public Gift saveGift(@RequestBody Gift gift, @PathVariable("id") Long giftChainId, Principal principal) throws Exception {
        User user = userRepo.findByUsername(principal.getName());
        GiftChain chain = giftChainRepo.findById(giftChainId);
        if (gift.getId() != null) {
            User giftUser = giftRepo.findById(gift.getId()).getUser();
            if (giftUser != null && giftUser.getId() != user.getId()) {
                throw new Exception("You can only modify your own gifts.");
            }
        }
        if (user.getId() != chain.getUser().getId()) {
            throw new Exception("You can only modify your own gift chains.");
        }
        gift.setGiftChain(chain);
        gift.setUser(user);
        giftRepo.save(gift);
        return gift;
    }

   @RequestMapping(
       value = "/gifts/{id}/upload",
       produces="application/json",
       consumes="multipart/form-data",
       method=RequestMethod.POST
   )
   @ResponseBody
   public ResponseEntity<String> handleFileUpload(
           @PathVariable("id") Long id,
           @RequestPart("file") MultipartFile multipartFile,
           Principal principal
   ) throws Exception {
       User user = userRepo.findByUsername(principal.getName());
       GiftFileManager mgr = GiftFileManager.get();
       Gift g = giftRepo.findById(id);
       if (g.getUser().getId() == user.getId()) {
           String imagePath = mgr.saveGiftData(giftRepo.findById(id), multipartFile);
           g.setImagePath(imagePath);
           giftRepo.save(g);
           return new ResponseEntity<String>(HttpStatus.OK);
       } else {
           // send 403
           return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
       }
   }
}