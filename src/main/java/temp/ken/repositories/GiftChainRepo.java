package temp.ken.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import temp.ken.models.GiftChain;

import java.util.List;

@Repository
public interface GiftChainRepo extends PagingAndSortingRepository<GiftChain, Long> {
    public GiftChain findById(@Param("id") Long id);

    @Query(
            value = "SELECT * FROM gift_chains " +
                    "JOIN users on users.id = gift_chains.user_id " +
                    "WHERE (users.username = :username);",
            nativeQuery = true
    )
    public List<GiftChain> findByUsername(
            @Param("username") String username
    );
}
