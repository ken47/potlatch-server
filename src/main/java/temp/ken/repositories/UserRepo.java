package temp.ken.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import temp.ken.models.User;

import java.util.Set;

@Repository
public interface UserRepo extends CrudRepository<User, Long> {
    public User findByUsername(@Param("username") String username);

    @Query(value = "SELECT * FROM users ORDER BY touched_count DESC LIMIT 20", nativeQuery = true)
    public Set<User> findTwentyMostTouched();

//    @Query(value = "SELECT * FROM users WHERE email = :email", nativeQuery = true)
//    User findByCustom(@Param("email") String email);
}
