package temp.ken.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;
import temp.ken.models.Gift;

import java.util.ArrayList;
import java.util.List;

// TODO change this to a paging repo
@RepositoryRestResource(path = "/gifts")
public interface GiftRepo extends PagingAndSortingRepository<Gift, Long> {
    public ArrayList<Gift> findByGiftChainId(@Param("gift_chain_id") Long giftChainId);

    public Gift findById(@Param("id") Long id);

    @Transactional
    @Modifying
    @Query(
            value = "BEGIN;\n" +
                    "INSERT INTO users_x_gift_touches (user_id, gift_id) VALUES (:userId, :giftId);\n" +
                    "UPDATE users SET touched_count = touched_count + 1 WHERE (id = :giftUserId);\n" +
                    "UPDATE gifts SET touched_count = touched_count + 1 WHERE (id = :giftId);\n" +
                    "COMMIT;",
            nativeQuery = true
    )
    public void touchGift(
            @Param("userId") Long userId,
            @Param("giftId") Long giftId,
            @Param("giftUserId") Long giftUserId
    );

    @Transactional
    @Modifying
    @Query(
            value = "INSERT INTO user_gift_locks (user_id, gift_id, operation, created) VALUES (:userId, :giftId, :operation, :timestamp);",
            nativeQuery = true
    )
    public void setLock(
            @Param("userId") Long userId,
            @Param("giftId") Long giftId,
            @Param("timestamp") Long timestamp,
            @Param("operation") String operation
    );

    @Query(
            value = "SELECT * FROM gifts " +
                    "JOIN users on users.id = gifts.user_id " +
                    "WHERE (users.username = :username);",
            nativeQuery = true
    )
    public List<Gift> findByUsername(
            @Param("username") String username
    );

    @Query(
            value = "SELECT * FROM users_x_gift_touches WHERE user_id = :userId AND (gift_id = :giftId);",
            nativeQuery = true
    )
    public Object hasUserTouched(
        @Param("userId") Long userId,
        @Param("giftId") Long giftId
    );

    @Query(
            value = "SELECT * FROM users_x_gift_flags WHERE user_id = :userId AND (gift_id = :giftId);",
            nativeQuery = true
    )
    public Object hasUserFlagged(
            @Param("userId") Long userId,
            @Param("giftId") Long giftId
    );

    public List<Gift> findByTitleLikeIgnoreCase(@Param("title") String title);

    @Query(
            value = "SELECT * FROM gifts ORDER BY id DESC;",
            nativeQuery = true
    )
    public List<Gift> getRecentGifts();

    // TODO: untested
    @Query(
            value = "SELECT COUNT(*) FROM users_x_gift_touches WHERE user_id = :userId AND (gift_id = :giftId);",
            nativeQuery = true
    )
    public Long getTouchedCount(
            @Param("userId") Long userId,
            @Param("giftId") Long giftId
    );

    @Transactional
    @Modifying
    @Query(
            value = "BEGIN;\n" +
                    "DELETE FROM users_x_gift_touches WHERE user_id = :userId AND (gift_id = :giftId);\n" +
                    "UPDATE users SET touched_count = touched_count - 1 WHERE (id = :giftUserId);\n" +
                    "UPDATE gifts SET touched_count = touched_count - 1 WHERE (id = :giftId);\n" +
                    "COMMIT;",
            nativeQuery = true
    )
    public int unTouchGift(
            @Param("userId") Long userId,
            @Param("giftId") Long giftId,
            @Param("giftUserId") Long giftUserId
    );

    @Transactional
    @Modifying
    @Query(
            value = "DELETE FROM user_gift_locks WHERE user_id = :userId AND gift_id = :giftId AND operation = :operation AND (created < :timestamp);",
            nativeQuery = true
    )
    public int freeOldLock(
            @Param("userId") Long userId,
            @Param("giftId") Long giftId,
            @Param("timestamp") Long timestamp,
            @Param("operation") String operation
    );

    @Transactional
    @Modifying
    @Query(
            value = "DELETE FROM user_gift_locks WHERE user_id = :userId AND gift_id = :giftId AND (operation = :operation);",
            nativeQuery = true
    )
    public int freeLock(
            @Param("userId") Long userId,
            @Param("giftId") Long giftId,
            @Param("operation") String operation
    );

    @Query(
            value = "SELECT * FROM gifts ORDER BY id DESC LIMIT 10;",
            nativeQuery = true
    )
    public List<Gift> findRecentGifts();

    @Query(
            value = "SELECT COUNT(*) FROM users_x_gift_flags WHERE user_id = :userId AND (gift_id = :giftId);",
            nativeQuery = true
    )
    public Long getFlaggedCount(
            @Param("userId") Long userId,
            @Param("giftId") Long giftId
    );

    @Transactional
    @Modifying
    @Query(
            value = "BEGIN;\n" +
                    "INSERT INTO users_x_gift_flags (user_id, gift_id) VALUES (:userId, :giftId);\n" +
                    "UPDATE users SET flagged_count = flagged_count + 1 WHERE (id = :giftUserId);\n" +
                    "UPDATE gifts SET flagged_count = flagged_count + 1 WHERE (id = :giftId);\n" +
                    "COMMIT;",
            nativeQuery = true
    )
    public int flagGift(
            @Param("userId") Long userId,
            @Param("giftId") Long giftId,
            @Param("giftUserId") Long giftUserId
    );

    @Transactional
    @Modifying
    @Query(
            value = "BEGIN;\n" +
                    "DELETE FROM users_x_gift_flags WHERE user_id = :userId AND (gift_id = :giftId);\n" +
                    "UPDATE users SET flagged_count = flagged_count - 1 WHERE (id = :giftUserId);\n" +
                    "UPDATE gifts SET flagged_count = flagged_count - 1 WHERE (id = :giftId);\n" +
                    "COMMIT;",
            nativeQuery = true
    )
    public void unFlagGift(
            @Param("userId") Long userId,
            @Param("giftId") Long giftId,
            @Param("giftUserId") Long giftUserId
    );
}