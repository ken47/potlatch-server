package temp.ken.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import temp.ken.models.User;

@Repository
// !! Type param User is just a dummy to satisfy Spring's bootup checks
public interface OAuthTokenRepo extends CrudRepository<User, Long> {
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM oauth_access_token WHERE user_name = :userName", nativeQuery = true)
    public void deleteToken(@Param("userName") String userName);
}
