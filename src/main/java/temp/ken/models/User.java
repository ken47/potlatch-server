package temp.ken.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This class represents a Category of a Video. Each Video can
 * be in exactly one Category (e.g., romance, action, horror, etc.).
 * This version of the class uses @OneToMany.
 *
 * @author jules
 *
 */
@Entity
@Table(name = "users")
public class User extends BaseEntity implements UserDetails {
    public static class UserSaltSource implements SaltSource {
        @Override
        public Object getSalt(UserDetails user) {
            return ((User) user).getSalt();
        }
    }

    private static final long serialVersionUID = 0L;

    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    public boolean isEnabled() {
        return true;
    }

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<Gift> gifts = new HashSet<Gift>();
    @JsonIgnore
    public Collection<Gift> getGifts() {
        return gifts;
    }

    public void setGifts(Set<Gift> gifts) {
        this.gifts = gifts;
    }

    public void addGift(Gift gift) {
        gifts.add(gift);
    }

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @RestResource(exported = false)
    private Set<GiftChain> giftChains = new HashSet<GiftChain>();
    @JsonIgnore
    public Collection<GiftChain> getGiftChains() {
        return giftChains;
    }

    public void setGiftChains(Set<GiftChain> giftChains) {
        this.giftChains = giftChains;
    }

    public void addGiftChain(GiftChain chain) {
        giftChains.add(chain);
    }

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private Set<Authority> authorities = new HashSet<Authority>();
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        authorities.add(new Authority("ROLE_USER"));
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public void addAuthorities(Authority authority) {
        authorities.add(authority);
    }

    @Column(name = "touched_count")
    private Long touchedCount = 0L;

    public Long getTouchedCount() {
        return touchedCount;
    }

    public void setTouchedCount(Long touchedCount) {
        this.touchedCount = touchedCount;
    }

    @Column(name = "flagged_count")
    private Long flaggedCount = 0L;

    public Long getFlaggedCount() {
        return flaggedCount;
    }

    public void setFlaggedCount(Long flaggedCount) {
        this.flaggedCount = flaggedCount;
    }

    @Column(name = "should_hide_flagged")
    private Boolean shouldHideFlagged = false;

    public Boolean getShouldHideFlagged() {
        return shouldHideFlagged;
    }

    public void setShouldHideFlagged(Boolean shouldHideFlagged) {
        this.shouldHideFlagged = shouldHideFlagged;
    }

    @Column(name = "salt")
    @NotEmpty
    private String salt;

    @JsonIgnore
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Column(name = "email", unique = true)
    @NotEmpty
    private String email;

    @JsonIgnore
    public String getEmail() {
        return email;
    }

    @JsonProperty
    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "update_interval")
    @NotNull
    private Short updateInterval = 60;

    public Short getUpdateInterval() {
        return updateInterval;
    }

    public void setUpdateInterval(Short updateInterval) {
        this.updateInterval = updateInterval;
    }

    // refers to a user gift
    @Column(name = "profile_pic_id")
    private Long profilePicId;

    public Long getProfilePicId() {
        return profilePicId;
    }

    public void setProfilePicId(Long profilePicId) {
        this.profilePicId = profilePicId;
    }

    @Column(name = "password")
    @NotEmpty
    private String password;

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "username", unique = true)
    @NotEmpty
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int hashCode() {
        return Objects.hashCode(getId());
    }

    public boolean equals(Object other) {
        return getId() == ((User) other).getId();
    }
}
