package temp.ken.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;

import javax.persistence.*;

@Entity
@Table(name="gifts")
public class Gift extends BaseEntity {
    @ManyToOne
    @RestResource(rel = "gift_user")
    @JoinColumn(name = "user_id")
    private User user;
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public String getUsername() {
       return user.getUsername();
    }

    @Transient
    private boolean touchedByCurrentUser;
    public boolean isTouchedByCurrentUser() {
        return touchedByCurrentUser;
    }
    public void setTouchedByCurrentUser(boolean touchedByCurrentUser) {
        this.touchedByCurrentUser = touchedByCurrentUser;
    }

    @Transient
    private boolean flaggedByCurrentUser;
    public void setFlaggedByCurrentUser(boolean flaggedByCurrentUser) {
        this.flaggedByCurrentUser = flaggedByCurrentUser;
    }
    public boolean isFlaggedByCurrentUser() {
        return flaggedByCurrentUser;
    }

    @ManyToOne
    @JoinColumn(name = "gift_chain_id")
    private GiftChain giftChain;
    @JsonIgnore
    public GiftChain getGiftChain() {
        return giftChain;
    }
    @JsonProperty
    public void setGiftChain(GiftChain giftChain) {
        this.giftChain = giftChain;
    }
    public String getGiftChainName() {
        return (null != giftChain) ? giftChain.getName() : "";
    }

    @Column(name = "title")
    @NotEmpty
    private String title;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "image_path")
    private String imagePath;
    public String getImagePath() {
        return imagePath;
    }
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Column(name = "caption")
    private String caption;
    public String getCaption() {
        return caption;
    }
    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Column(name = "touched_count")
    private Long touchedCount = 0L;
    public Long getTouchedCount() {
        return touchedCount;
    }
    public void setTouchedCount(Long touchedCount) {
        this.touchedCount = touchedCount;
    }

    @Column(name = "flagged_count")
    private Long flaggedCount = 0L;
    public Long getFlaggedCount() {
        return flaggedCount;
    }
    public void setFlaggedCount(Long flaggedCount) {
        this.flaggedCount = flaggedCount;
    }

    @Override
    public boolean equals(final Object _other) {
        Gift other = (Gift) _other;
        return hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getUser(), getTitle());
    }
}
