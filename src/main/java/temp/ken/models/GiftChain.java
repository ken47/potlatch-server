package temp.ken.models;

import com.google.common.base.Objects;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;

/**
 * Created by kenster on 11/27/14.
 */
@Entity
@Table(name="gift_chains")
public class GiftChain extends BaseEntity {
    @Column(name = "name")
    @NotEmpty
    private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @RestResource(rel = "gift_chain_user")
    @JoinColumn(name = "user_id")
    private User user;
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(final Object _other) {
        GiftChain other = (GiftChain) _other;
        return hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getUser(), getName());
    }
}
