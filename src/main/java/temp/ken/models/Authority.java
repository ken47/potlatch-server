package temp.ken.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Objects;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Created by ken47 on 11/16/14.
 */
@Entity
@Table(name="authorities")
public class Authority extends BaseEntity implements GrantedAuthority {
    // empty constructor needed for Spring persistence
    public Authority() {}

    public Authority(String authority) {
        this.authority = authority;
    }

    @Column(name = "authority")
    @NotEmpty
    private String authority;

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @ManyToOne
    @RestResource(rel = "authority_user")
    @JoinColumn(name = "user_id")
    private User user;
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
